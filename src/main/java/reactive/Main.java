package reactive;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class Main {

  public static void main(String[] args) throws InterruptedException, ExecutionException {
    System.out.println("Start");
    Future<String> stringFuture = calculateAsync();

    Thread.sleep(5000);
    System.out.println("Hello");

    String s = stringFuture.get();
    System.out.println(s);

  }

  private static Future<String> calculateAsync() {
    CompletableFuture<String> completableFuture = new CompletableFuture<>();

    Executors.newCachedThreadPool().submit(() -> {
      Thread.sleep(5000);
      return completableFuture.complete("World");
    });

    return completableFuture;
  }

}
