package hexagonalarchitecture.specificationpattern;

import hexagonalarchitecture.specificationpattern.genericspecification.AbstractSpecification;

//The String of the abstract specification should be a domain object
public class NetworkAvailabilitySpecification extends AbstractSpecification<String> {
  private static final int MAXIMUM_ALLOWED_NETWORKS = 6;
  @Override
  public boolean isSatisfiedBy(String networks) {
    return networks.length() > MAXIMUM_ALLOWED_NETWORKS;
  }
}
