package hexagonalarchitecture.specificationpattern;

import hexagonalarchitecture.specificationpattern.genericspecification.AbstractSpecification;

//The Integer of the abstract specification should be a domain object
public class CIDRSpecification extends AbstractSpecification<Integer> {
  public static final int MINIMUM_ALLOWED_CIDR = 8;
  @Override
  public boolean isSatisfiedBy(Integer cidr) {
    return cidr > MINIMUM_ALLOWED_CIDR;
  }
}
