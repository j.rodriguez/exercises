package hexagonalarchitecture.specificationpattern;


public class NetworkOperation {

  public void createNewNetwork(int cidr, String networks) {
    NetworkAvailabilitySpecification availabilitySpec = new NetworkAvailabilitySpecification();
    CIDRSpecification cidrSpec = new CIDRSpecification();
    RouterTypeSpecification routerTypeSpec = new RouterTypeSpecification();
    if (cidrSpec.isSatisfiedBy(cidr)) {
      throw new IllegalArgumentException("CIDR is below" + CIDRSpecification.MINIMUM_ALLOWED_CIDR);
    }
    if (availabilitySpec.isSatisfiedBy(networks)) {
      throw new IllegalArgumentException("Address already exist");
    }
    if (availabilitySpec.and(routerTypeSpec).isSatisfiedBy("routerType")) {
      System.out.println("Network can be created");
    }
  }
}
