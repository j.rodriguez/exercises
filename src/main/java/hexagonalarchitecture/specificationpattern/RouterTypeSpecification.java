package hexagonalarchitecture.specificationpattern;

import hexagonalarchitecture.specificationpattern.genericspecification.AbstractSpecification;

//The String of the abstract specification should be a domain object
public class RouterTypeSpecification extends AbstractSpecification<String> {

  @Override
  public boolean isSatisfiedBy(String router) {
    return
      router.equals("EDGE") || router.equals("CORE");
  }
}
