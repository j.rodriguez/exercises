package exercises;

import java.util.Objects;

/**
 * @author jrodriguez
 */
public class FindSumZeroInArray {
    int[] array;

    public FindSumZeroInArray(int[] array) {
        this.array = array;
        Objects.requireNonNull(array);
    }

    public int[] find() {
        for (int i = 0; i < this.array.length; i++) {
            int[] arrayToSum = getSubArray(this.array, i);
            int sum = 0;
            for (int j = 0; j < arrayToSum.length; j++) {
                sum = arrayToSum[j] + sum;
                if (sum == 0) return getFinalSubArray(arrayToSum, j);
            }
        }
        return new int[]{};
    }

    private int[] getFinalSubArray(int[] array, int index) {
        int[] subArray = new int[index + 1];
        int j = 0;
        for (int i = 0; i < index + 1; i++) {
            subArray[j] = array[i];
            j += 1;
        }
        return subArray;
    }

    private int[] getSubArray(int[] array, int index) {
        int[] subArray = new int[array.length - index];
        int j = 0;
        for (int i = index; i < array.length; i++) {
            subArray[j] = array[i];
            j += 1;
        }
        return subArray;
    }

/*
    public int[] find() {
        for (int i = 0; i < this.array.length; i++) {
            List<Integer> finalArray = new ArrayList<>();
            int sum = this.array[i];
            finalArray.add(this.array[i]);
            if (sum == 0) {
                return convertToArray(finalArray);
            }

            for (int j = i + 1; j < this.array.length; j++) {
                sum = sum + this.array[j];
                finalArray.add(this.array[j]);
                if (sum == 0) {
                    return convertToArray(finalArray);
                }
            }
        }
        return new int[]{};
    }

    private int[] convertToArray(List<Integer> finalArrayList) {
        int[] finalArray = new int[finalArrayList.size()];
        for (int i = 0; i < finalArrayList.size(); i++) {
            finalArray[i] = finalArrayList.get(i);
        }
        return finalArray;
    }*/
}
