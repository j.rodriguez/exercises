package exercises;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

/**
 * @author jrodriguez
 */
public class Ecuations {
    public static Integer SystemofEquations(String str) {
        // code goes here
        List<String> ecuations = Arrays.asList(str.split(";"));
        if (findSameVarOnLeftMultipleTimes(ecuations)) return null;
        List<Equation> equations = new ArrayList<>();
        for (String e : ecuations) {
            String[] split = e.split("=");
            if (split.length != 2) return null;
            String left = split[0];
            String right = split[1];
            equations.add(new Equation(left.trim(), right.trim().replace(" ", "")));
        }
        resolveFunctions(equations);
        Equation xEquation = equations.stream().filter(f -> "x".equals(f.getLeft())).findFirst().orElse(null);
        if (xEquation != null) {
            return xEquation.getResult();
        } else {
            return null;
        }
    }

    public static void main(String[] args) {
        // keep this function call here
        SystemofEquations("c = 3717 ^ 8396 ^ 4987; x = c ^ d ^ d ^ u; u = d ^ c; d = 4144");
    }

    private static void resolveFunctions(List<Equation> equations) {
        boolean xShouldBeResolved = false;
        int tries = 0;
        while (!xShouldBeResolved) {
            for (Equation equation : equations) {
                if (equation.getResult() == null) {
                    resolveFunction(equation, equations);
                }
            }
            Equation xEquation = equations.stream().filter(f -> "x".equals(f.getLeft())).findFirst().orElse(null);
            if (xEquation != null) {
                if (xEquation.isResolved()) {
                    xShouldBeResolved = true;
                }
            } else {
                xShouldBeResolved = true;
            }
            if (tries == equations.size()) xShouldBeResolved = true;
            tries += 1;
        }
    }

    private static void resolveFunction(Equation equation, List<Equation> equations) {
        String[] operations = equation.getRight().split("\\^");
        int result = 0;
        boolean resolved = true;
        for (int i = 0; i < operations.length; i++) {
            String stringOp = operations[i];
            int operator;
            try {
                operator = Integer.parseInt(stringOp);
            } catch (NumberFormatException ex) {
                Equation operatorEquation = equations.stream().filter(f -> stringOp.equals(f.getLeft())).findFirst().orElse(null);
                if (operatorEquation.getResult() != null) {
                    operator = operatorEquation.getResult();
                } else {
                    resolved = false;
                    break;
                }
            }
            if (i == 0) {
                result = operator;
            } else {
                result = result ^ operator;
            }
        }
        if (resolved) {
            equation.setResult(result);
            equation.setResolved(true);
        }
    }


    private static boolean findSameVarOnLeftMultipleTimes(List<String> ecuations) {
        List<String> leftSideOfEquations = new ArrayList<>();
        for (String e : ecuations) {
            String s = e.split("=")[0];
            if (leftSideOfEquations.contains(s)) {
                return true;
            }
            leftSideOfEquations.add(s);
        }
        return false;
    }
}

class Equation {
    private final String left;
    private final String right;
    private int result;
    private boolean isResolved = false;

    public Equation(String left, String right) {
        Objects.requireNonNull(left);
        Objects.requireNonNull(right);
        this.left = left;
        this.right = right;
    }

    public String getLeft() {
        return left;
    }

    public String getRight() {
        return right;
    }

    public Integer getResult() {
        return result;
    }

    public void setResult(int result) {
        this.result = result;
    }

    public void setResolved(boolean resolved) {
        isResolved = resolved;
    }

    public boolean isResolved() {
        return isResolved;
    }
}
