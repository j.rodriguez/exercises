package exercises;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author jrodriguez
 */
public class ProcessArrays {
    private final int[] arr;

    public ProcessArrays(int[] arr) {
        this.arr = arr;
    }

    public int[][] processArraysArrayApproach() {
        //declare final object
        int[][] finalArray = new int[dynamicArraySize()][2];

        //find and count duplicates in initial array
        for (int i = 0; i < arr.length; i++) {
            int count = 1;
            int value = arr[i];

            //stop processing if key is already present in final array
            if (!containsKey(value, finalArray)) {
                for (int j = 0; j < arr.length; j++) {
                    //count only if values match in a distinct index
                    if (value == arr[j] && i != j) {
                        count++;
                    }
                }
                int[] array = new int[2];
                array[0] = value;
                array[1] = count;
                finalArray[i] = array;
            }
        }
        return finalArray;
    }

    public Map<String, Integer> processArraysMapApproach() {
        //declare final object
        Map<String, Integer> finalMap = new LinkedHashMap<>();

        //find and count duplicates in initial array
        for (int i = 0; i < arr.length; i++) {
            int count = 1;
            int value = arr[i];

            //stop processing if key is already present in final map
            if (!containsKey(value, finalMap)) {
                for (int j = 0; j < arr.length; j++) {
                    //count only if values match in a distinct index
                    if (value == arr[j] && i != j) {
                        count++;
                    }
                }
                finalMap.put(String.valueOf(value), count);
            }
        }
        return finalMap;
    }

    private boolean containsKey(int value, Object[] finalArray) {
        for (Object o : finalArray) {
            int[] temp = (int[]) o;
            if (temp == null) {
                return false;
            }
            int compare = temp[0];
            if (value == compare) {
                return true;
            }
        }
        return false;
    }

    private boolean containsKey(int value, Map<String, Integer> finalMap) {
        return finalMap.get(String.valueOf(value)) != null;
    }

    private int dynamicArraySize() {
        int counter = 1;
        for (int i = 0; i < arr.length; i++) {
            int compareValue = arr[i];
            boolean exists = false;
            for (int j = 0; j < arr.length; j++) {
                if (i != j && compareValue == arr[j]) {
                    exists = true;
                    break;
                }
            }
            if (!exists) counter++;
        }
        return counter;
    }
}
