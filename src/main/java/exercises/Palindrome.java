package exercises;

/**
 * @author jrodriguez
 */
public class Palindrome {
    private final String text;

    public Palindrome(String text) {
        this.text = text;
    }

    public boolean isPalindrome() {
        StringBuilder reversedText = new StringBuilder();
        for (int i = 0; i < text.length(); i++) {
            char c = text.charAt(i);
            reversedText.insert(0, c);
        }
        return reversedText.toString().equals(text);
    }
}
