package exercises;

import java.util.Arrays;
import java.util.Objects;

/**
 * @author jrodriguez
 */
public class SortArray {
    int[] array;

    public SortArray(int[] array) {
        this.array = array;
        Objects.requireNonNull(array);
    }

    public int[] sort() {
        Arrays.sort(array);
        return array;
        /*int n = array.length;
        for (int i = 0; i < n - 1; i++)
            for (int j = 0; j < n - i - 1; j++)
                if (array[j] > array[j + 1]) {
                    int temp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = temp;
                }
        return array;*/
    }
}
