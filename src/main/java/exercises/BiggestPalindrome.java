package exercises;

import java.util.Objects;

/**
 * @author jrodriguez
 */
public class BiggestPalindrome {
    private final String text;
    private final String reversedString;

    public BiggestPalindrome(String text) {
        Objects.requireNonNull(text);
        this.text = text;
        this.reversedString = reverseString(this.text);
    }

    public String findBiggestPalindrome() {
        for (int i = 0; i < text.length() / 2; i++) {
            String substring = text.substring(i, text.length() - i);
            String reversedSubstring = reversedString.substring(i, reversedString.length() - i);

            for (int j = 0; j < substring.length(); j++) {
                String potentialPalindrome = substring.substring(0, substring.length() - j);
                if (potentialPalindrome.length() > 1) {
                    if (potentialPalindrome.equals(reverseString(potentialPalindrome))) return potentialPalindrome;

                    potentialPalindrome = reversedSubstring.substring(0, reversedSubstring.length() - j);
                    if (potentialPalindrome.equals(reverseString(potentialPalindrome))) return potentialPalindrome;
                }
            }
        }
        return null;
    }

    /*public String findBiggestPalindrome() {
        for (int i = 0; i < this.text.length(); i++) {
            String comparingSubstring = this.text.substring(i);
            String reversedComparingSubstring = this.reversedString.substring(i);

            for (int j = 0; j < comparingSubstring.length(); j++) {
                if (j != comparingSubstring.length() - 1) {
                    String toCompareSubstring;
                    String reversedToCompareSubstring;

                    toCompareSubstring = comparingSubstring.substring(0, comparingSubstring.length() - j);
                    reversedToCompareSubstring = reverseString(toCompareSubstring);
                    if (toCompareSubstring.equals(reversedToCompareSubstring)) return toCompareSubstring;

                    toCompareSubstring = reversedComparingSubstring.substring(0, reversedComparingSubstring.length() - j);
                    reversedToCompareSubstring = reverseString(toCompareSubstring);
                    if (toCompareSubstring.equals(reversedToCompareSubstring)) return toCompareSubstring;
                }
            }
        }
        return null;
    }*/

    private String reverseString(String text) {
        StringBuilder reversedSubstring = new StringBuilder();

        for (int j = 0; j < text.length(); j++) {
            reversedSubstring.insert(0, text.charAt(j));
        }

        return reversedSubstring.toString();
    }
}
