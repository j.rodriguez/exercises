package exercises;

import java.util.Objects;

/**
 * @author jrodriguez
 */
public class RepetitiveString {
    private String text;

    public RepetitiveString(String text) {
        Objects.requireNonNull(text);
        this.text = text;
    }

    public String findRepetitiveString() {
        int slices = 2;
        for (int i = slices; i <= text.length(); i++) {
            if (text.length() % slices == 0) {
                int cut = text.length() / slices;
                String compare = text.substring(0, cut);
                boolean equals = true;
                for (int j = 1; j < slices; j++) {
                    String compareWith = text.substring(cut, cut + compare.length());
                    if (!compare.equals(compareWith)) {
                        equals = false;
                        break;
                    }
                    cut += compare.length();
                }
                if (equals) return compare;
            }
            slices += 1;
        }
        return "none";
    }








    /*public String findRepetitiveString() {
        int cutoff = 0;
        int divideBy = 2;

        //loop until there are no more divisions to be done
        while (cutoff != 1) {

            //calculate cutoff to start comparing substrings
            cutoff = text.length() / divideBy;
            if (this.text.length() % divideBy == 0) {
                String persistCompare = this.text.substring(0, cutoff);
                boolean found = true;

                //iterate over the string as much times as possible
                for (int i = 0; i < divideBy - 1; i++) {
                    int start = cutoff;
                    int end = start + persistCompare.length();
                    String b = this.text.substring(start, end);

                    //if not equals, break for and found false
                    if (!persistCompare.equals(b)) {
                        found = false;
                        break;
                    }
                    cutoff = cutoff + persistCompare.length();
                }
                if (found) return persistCompare;
            }
            divideBy = divideBy + 1;
        }
        return "none";
    }*/

    /*public String findRepetitiveString() {
        int slices = 2;
        for (int i = 0; i < this.text.length(); i++) {
            if (this.text.length() % slices == 0) {
                int cutoff = this.text.length() / slices;
                if (cutoff == 1 && this.text.length() != slices) break;
                String compare = this.text.substring(0, cutoff);
                boolean equals = true;
                for (int j = 1; j < slices; j++) {
                    String toCompare = this.text.substring(cutoff, cutoff + compare.length());
                    if (!compare.equals(toCompare)) {
                        equals = false;
                        break;
                    }
                    cutoff += compare.length();
                }
                if (equals) return compare;
            }
            slices += 1;
        }
        return "none";
    }*/
}