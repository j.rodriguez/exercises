package tdd.strongpassword;

public class StrongPassword {

  private final String password;

  public StrongPassword(String password) {
    this.password = password;
  }

  public boolean isPasswordStrong() {
    return password.contains("_") && password.length() >= 6 && hasANumber() && hasAnUppercaseLetter() && hasALowercaseLetter();
  }

  private boolean hasANumber() {
    return password.chars().anyMatch(c -> Character.isDigit((char) c));
  }
  private boolean hasAnUppercaseLetter() {
    return password.chars().filter(c -> Character.isLetter((char) c)).anyMatch(c -> !Character.isLowerCase((char) c));
  }

  private boolean hasALowercaseLetter() {
    return password.chars().filter(c -> Character.isLetter((char) c)).anyMatch(c -> Character.isLowerCase((char) c));
  }

}
