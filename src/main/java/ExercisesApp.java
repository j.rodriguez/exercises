import java.util.*;

/**
 * @author jrodriguez
 */
public class ExercisesApp {
    static int[] intArray = new int[]{-1, -5, 13, 14, 15, 0, 2, 80, 9, 0, -1};

    public static String Grouper(String str) {
        // code goes here
        Person[] persons = getPersonsList(str);
        Map<Integer, List<Person>> personsMap = new LinkedHashMap<>();
        for (Person person : persons) {
            List<Person> personList;
            if (personsMap.get(person.getAgeRange()) == null) {
                personList = new ArrayList<>();
                personsMap.put(person.getAgeRange(), personList);
            } else {
                personList = personsMap.get(person.getAgeRange());
            }
            personList.add(person);
        }
        StringBuilder message = new StringBuilder();
        for (Map.Entry<Integer, List<Person>> entry : personsMap.entrySet()) {
            message.append(entry.getKey()).append(":");
            for (Person person : entry.getValue()) {
                message.append(person.getConcat()).append(";");
            }
        }
        message.deleteCharAt(message.length() - 1);
        return message.toString();
    }

    private static Person[] getPersonsList(String str) {
        List<String> persons = Arrays.asList(str.split(";"));
        Person[] personList = new Person[persons.size()];

        for (int i = 0; i < persons.size(); i++) {
            String[] split = persons.get(i).split("-");
            personList[i] = new Person(split[0], Integer.parseInt(split[1]));
        }
        //sort
        Arrays.sort(personList, Comparator.comparing(Person::getAge).thenComparing(Person::getName));
        return personList;
    }

    public static void main(String[] args) {
        // keep this function call here
        Grouper("Anna-22;Nora-31;Peter-18;Sam-30;Simon-22;Larry-25;John-24");
    }
}

class Person {
    private String name;
    private int age;
    private int ageRange;

    public Person(String name, int age) {
        Objects.requireNonNull(name);
        this.name = name;
        this.age = age;
        this.ageRange = calculateAgeRange(age);
    }

    public int getAgeRange() {
        return ageRange;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public String getConcat() {
        return this.name + "-" + this.age;
    }

    private static int calculateAgeRange(int age) {
        if (age < 10) return 0;
        int lastNumber = Integer.parseInt(String.valueOf(String.valueOf(age).charAt(String.valueOf(age).length() - 1)));
        return age - lastNumber;
    }
}
