package lambdas.logging;

import java.security.InvalidParameterException;

public class DoThing {

  private Logger logger;
  private boolean a;

  public DoThing(Logger logger) {
    this.logger = logger;
  }

  public void doThing() {
    logger.log(() -> doThingWithoutLogging("a"));
  }

  public void doThingWithoutLogging(String a) {
    System.out.println(a.length());
  }

  public void test() {

  }

  private Function wrapLogging(Function f) {
    Function function = (a) -> {
      try {
        f.function(a);
      } catch (Exception e) {
        throw new InvalidParameterException(e.getMessage());
      }
    };

    return function;
  }

  public void wrapIt(String a) {
    Function doThingWithLogging = wrapLogging(this::doThingWithoutLogging);
    doThingWithLogging.function(a);
  }

}
