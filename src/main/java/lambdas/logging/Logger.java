package lambdas.logging;

public class Logger {

  public void log(LoggingAction action) {
    try {
      action.action();
    } catch (Exception e) {
      System.out.println(e.getMessage());
    }
  }

}
