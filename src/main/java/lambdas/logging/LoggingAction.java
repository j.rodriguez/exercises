package lambdas.logging;

public interface LoggingAction {

  void action();
}
