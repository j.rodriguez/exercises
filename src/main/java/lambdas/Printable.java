package lambdas;

public interface Printable {

  boolean print(String text);

}
