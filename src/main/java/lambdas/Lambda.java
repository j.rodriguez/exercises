package lambdas;

import java.util.stream.Stream;

public class Lambda {

  public boolean lambda() {
    Printable hello = (text) -> {
      System.out.println(text + "!");
      return text.equalsIgnoreCase("test");
    };

    return printThing(hello);
  }

  public void streamExample() {
    Stream<String> stream = Stream.of("a", "b", "c").filter(element -> element.contains("b"));
    Stream<String> twiceModifiedStream = stream.skip(1).map(element -> element.substring(0, 3));
  }

  private boolean printThing(Printable printable) {
    return printable.print("Hello Jon");
  }

  private void withLogging(Runnable doStaff) {
    try {
      doStaff.run();
    } catch (Exception e) {
      System.out.println("log");
    }
  }

  public void doThing() {
    Runnable f = () -> {
      System.out.println("done");
    };

    //locally defined
    withLogging(f);

    //inline defined
    withLogging(() -> System.out.println("hello"));

    //globally defined
    withLogging(this::act);
  }

  private void act() {
    System.out.println("hello");
  }


}