package prints;

/**
 * @author jrodriguez
 */
public class PrintExercises {
    private String name;

    public PrintExercises(String name) {
        this.name = name;
    }

    public void print() {
        System.out.println("Hello" + "\n" + this.name);
    }
}
