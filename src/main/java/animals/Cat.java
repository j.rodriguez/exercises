package animals;

/**
 * @author jrodriguez
 */
public class Cat {
    String sound;

    String getSound() {
        return sound;
    }

    void setSound(String sound) {
        this.sound = sound;
    }
}
