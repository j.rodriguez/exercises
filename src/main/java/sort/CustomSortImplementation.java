package sort;

import java.util.ArrayList;
import java.util.List;

/**
 * @author jrodriguez
 */
public class CustomSortImplementation implements ListSort {

    @Override
    public List<Integer> sort(List<Integer> list) {
        List<Integer> sortedList = new ArrayList<>();
        for (int newValue : list) {
            int targetIndex = -1;
            for (int j = 0; j < sortedList.size(); j++) {
                int compareValue = sortedList.get(j);
                if (newValue < compareValue) {
                    targetIndex = j;
                    break;
                }
            }
            if (targetIndex != -1) {
                sortedList.add(targetIndex, newValue);
            } else {
                sortedList.add(newValue);
            }
        }
        return sortedList;
    }
}
