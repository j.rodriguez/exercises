package sort;

import java.util.List;

/**
 * @author jrodriguez
 */
public interface ListSort {
    List<Integer> sort(List<Integer> list);
}
