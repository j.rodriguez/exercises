package immutability;

public class ImmutableNumber {

  private final int someNumber;

  public ImmutableNumber(int someNumber) {
    this.someNumber = someNumber;
  }

  public int sumNumber(int anotherNumber) {
    return someNumber + anotherNumber;
  }
}
