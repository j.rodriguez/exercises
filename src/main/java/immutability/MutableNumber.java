package immutability;

public class MutableNumber {

  private int someNumber;

  public MutableNumber(int someNumber) {
    this.someNumber = someNumber;
  }

  public int sumNumber(int anotherNumber) {
    someNumber = someNumber + anotherNumber;
    return someNumber;
  }
}
