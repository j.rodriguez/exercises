package tdd.strongpassword;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import tdd.strongpassword.StrongPassword;

class StrongPasswordTest {

  @Test
  void passwordShouldContainUnderscore() {
    StrongPassword sut = new StrongPassword("passwordWithoutUnderscore");

    boolean passwordWithUnderscore = sut.isPasswordStrong();

    assertThat(passwordWithUnderscore).isFalse();
  }

  @Test
  void passwordShouldHaveAtLeastSixCharacters() {
    StrongPassword sut = new StrongPassword("p_w");

    boolean passwordWithUnderscore = sut.isPasswordStrong();

    assertThat(passwordWithUnderscore).isFalse();
  }

  @Test
  void passwordShouldContainANumber() {
    StrongPassword sut = new StrongPassword("password_without_number");

    boolean passwordWithUnderscore = sut.isPasswordStrong();

    assertThat(passwordWithUnderscore).isFalse();
  }

  @Test
  void passwordShouldContainAnUppercaseLetter() {
    StrongPassword sut = new StrongPassword("password_without_capital_letter_7");

    boolean passwordWithUnderscore = sut.isPasswordStrong();

    assertThat(passwordWithUnderscore).isFalse();
  }

  @Test
  void passwordShouldContainALowercaseLetter() {
    StrongPassword sut = new StrongPassword("PASSWORD_WITHOUT_CAPITAL_LETTER_7");

    boolean passwordWithUnderscore = sut.isPasswordStrong();

    assertThat(passwordWithUnderscore).isFalse();
  }

  @Test
  void validPassword() {
    StrongPassword sut = new StrongPassword("vaLid_passWord1");

    boolean passwordWithUnderscore = sut.isPasswordStrong();

    assertThat(passwordWithUnderscore).isTrue();
  }

}
