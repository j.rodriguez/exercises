package exercises;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

import org.junit.jupiter.api.Test;

/**
 * @author jrodriguez
 */
public class FindSumZeroInArrayTest {

    @Test
    public void test1() {
        //setup
        int[] arr = new int[]{-1, 2, -1, 3, 5};
        int[] expected = new int[]{-1, 2, -1};

        //sut
        FindSumZeroInArray findSumZeroInArray = new FindSumZeroInArray(arr);
        int[] result = findSumZeroInArray.find();

        //assertions
        assertArrayEquals(expected, result);
    }

    @Test
    public void test2() {
        //setup
        int[] arr = new int[]{1, 2, 3, -1, 1};
        int[] expected = new int[]{-1, 1};

        //sut
        FindSumZeroInArray findSumZeroInArray = new FindSumZeroInArray(arr);
        int[] result = findSumZeroInArray.find();

        //assertions
        assertArrayEquals(expected, result);
    }

    //[0,2,3,5] -> 0
    @Test
    public void test3() {
        //setup
        int[] arr = new int[]{0, 2, 3, 5};
        int[] expected = new int[]{0};

        //sut
        FindSumZeroInArray findSumZeroInArray = new FindSumZeroInArray(arr);
        int[] result = findSumZeroInArray.find();

        //assertions
        assertArrayEquals(expected, result);
    }

    //[1,2,3,4,5] -> NO
    @Test
    public void test4() {
        //setup
        int[] arr = new int[]{1, 2, 3, 4, 5};
        int[] expected = new int[]{};

        //sut
        FindSumZeroInArray findSumZeroInArray = new FindSumZeroInArray(arr);
        int[] result = findSumZeroInArray.find();

        //assertions
        assertArrayEquals(expected, result);
    }

    //[1,1,1,1,1,5] -> [1,1,1,1,1,5]
    @Test
    public void test5() {
        //setup
        int[] arr = new int[]{1, 1, 1, 1, 1, -5};
        int[] expected = new int[]{1, 1, 1, 1, 1, -5};

        //sut
        FindSumZeroInArray findSumZeroInArray = new FindSumZeroInArray(arr);
        int[] result = findSumZeroInArray.find();

        //assertions
        assertArrayEquals(expected, result);
    }
}
