package exercises;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.HashSet;
import java.util.Set;
import org.junit.jupiter.api.Test;
import sum.SumExercise;

public class SumTest {

  @Test
  public void testingImplementationDetails() {
    SumExercise sut = new SumExercise();
    int a = 3;
    int b = 4;
    int expected = 7;

    int result = sut.sum(a, b);

    assertEquals(expected, result);
  }

  @Test
  public void testNullPointer() {

    Set<String> allMetrics = new HashSet<>();
    allMetrics.add("s1");
    allMetrics.add("s2");
    allMetrics.add("s3");

    allMetrics.containsAll(null);

    System.out.println("hello");

  }

}
