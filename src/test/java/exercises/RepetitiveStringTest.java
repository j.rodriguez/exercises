package exercises;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

/**
 * @author jrodriguez
 */
public class RepetitiveStringTest {

    @Test
    public void constructWithNullShouldThrowNullPointerException() {
        assertThrows(NullPointerException.class, () -> new RepetitiveString(null));
    }

    @Test
    public void input_abab_returns_ab() {
        //setup
        String text = "abab";
        String expected = "ab";

        //sut
        RepetitiveString repetitiveString = new RepetitiveString(text);

        //assertions
        assertEquals(expected, repetitiveString.findRepetitiveString());

    }

    @Test
    public void input_abababab_returns_abab() {
        //setup
        String text = "abababab";
        String expected = "abab";

        //sut
        RepetitiveString repetitiveString = new RepetitiveString(text);

        //assertions
        assertEquals(expected, repetitiveString.findRepetitiveString());

    }

    @Test
    public void input_abbjabbjabbj_returns_abbj() {
        //setup
        String text = "abbjabbjabbj";
        String expected = "abbj";

        //sut
        RepetitiveString repetitiveString = new RepetitiveString(text);

        //assertions
        assertEquals(expected, repetitiveString.findRepetitiveString());

    }

    @Test
    public void input_abjabjabj_returns_abj() {
        //setup
        String text = "abjabjabj";
        String expected = "abj";

        //sut
        RepetitiveString repetitiveString = new RepetitiveString(text);

        //assertions
        assertEquals(expected, repetitiveString.findRepetitiveString());

    }

    @Test
    public void input_abbjabbjabb_returns_none() {
        //setup
        String text = "abbjabbjabb";
        String expected = "none";

        //sut
        RepetitiveString repetitiveString = new RepetitiveString(text);

        //assertions
        assertEquals(expected, repetitiveString.findRepetitiveString());

    }

    @Test
    public void input_ababxabab_returns_none() {
        //setup
        String text = "ababxabab";
        String expected = "none";

        //sut
        RepetitiveString repetitiveString = new RepetitiveString(text);

        //assertions
        assertEquals(expected, repetitiveString.findRepetitiveString());

    }

    @Test
    public void test_input_abbjaabbjah_returns_none() {
        //setup
        String text = "abbjaabbjah";
        String expected = "none";

        //sut
        RepetitiveString repetitiveString = new RepetitiveString(text);

        //assertions
        assertEquals(expected, repetitiveString.findRepetitiveString());

    }

    @Test
    public void test_input_aa_returns_a() {
        //setup
        String text = "aa";
        String expected = "a";

        //sut
        RepetitiveString repetitiveString = new RepetitiveString(text);

        //assertions
        assertEquals(expected, repetitiveString.findRepetitiveString());

    }
}
