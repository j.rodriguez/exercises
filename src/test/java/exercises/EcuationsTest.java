package exercises;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import org.junit.jupiter.api.Test;

/**
 * @author jrodriguez
 */
public class EcuationsTest {
    @Test
    public void test1() {
        Ecuations e = new Ecuations();
        String ecuation = "c = 3717 ^ 8396 ^ 4987; x = c ^ d ^ d ^ u; u = d ^ c; d = 4144";
        Integer s = e.SystemofEquations(ecuation);
        assertEquals(4144, (int) s);
    }

    @Test
    public void test2() {
        Ecuations e = new Ecuations();
        String ecuation = "y = 6 ^ z; z = y ^ 2; x = z ^ y ^ 3";
        Integer s = e.SystemofEquations(ecuation);
        assertNull(s);
    }

    @Test
    public void test3() {
        Ecuations e = new Ecuations();
        String ecuation = "y = 6 ^ z; z = y ^ 2; x = z ^ y ^ 3; z = 3";
        Integer s = e.SystemofEquations(ecuation);
        assertNull(s);
    }
}
