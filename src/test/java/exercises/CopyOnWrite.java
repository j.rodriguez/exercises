package exercises;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.junit.jupiter.api.Test;

public class CopyOnWrite {


  @Test
  void copy_on_write_test() {
    List<Integer> list = new ArrayList<>();
    list.add(1);
    list.add(2);
    list.add(3);
    list.add(4);

    /*List<Integer> copy = new ArrayList<>(List.copyOf(list));

    copy.set(0, 5);
    System.out.println(list);
    System.out.println(copy);
    System.out.println(list.equals(copy));*/
    //assertThat(!list.equals(copy));
  }

  @Test
  void copy_on_write_test2() {
    int[] ints = {1, 2, 3, 4};
    int[] copy = Arrays.copyOf(ints, ints.length);

    copy[0] = 5;
    System.out.println(ints);
    System.out.println(copy);
    System.out.println(ints[0]);
    System.out.println(copy[0]);
    System.out.println(ints == copy);
    //assertThat(ints[0] != copy[0]);
  }

  @Test
  void copy_on_write_test3() {
    List<Person> list = new ArrayList<>();
    list.add(new Person("jon", "rodriguez", 31));
    list.add(new Person("andrea", "aldao", 29));
    list.add(new Person("a", "b", 3));
    list.add(new Person("c", "d", 5));
    /*List<Person> copy = new ArrayList<>(List.copyOf(list));
    System.out.println(list);
    System.out.println(copy);
    System.out.println(list.equals(copy));

    Person person = copy.get(0);
    person.setName("mikel");
    person.setSurname("elorza");
    person.setYears(30);
    //Person mikel = new Person("mikel", "elorza", 30);
    //copy.set(0, mikel);
    System.out.println(list.get(0).getName());
    System.out.println(copy.get(0).getName());
    System.out.println(list.equals(copy));*/
  }

  @Test
  void copy_on_write_test4() {
    List<Person> list = new ArrayList<>();
    list.add(new Person("jon", "rodriguez", 31));
    list.add(new Person("andrea", "aldao", 29));
    list.add(new Person("a", "b", 3));
    list.add(new Person("c", "d", 5));
    /*List<Person> copy = new ArrayList<>(List.copyOf(list));
    System.out.println(list);
    System.out.println(copy);
    System.out.println(list.equals(copy));

    Person copyOfPerson = new Person(copy.get(0).getName(), copy.get(0).getSurname(), copy.get(0).getYears());
    copyOfPerson.setName("jon");
    copyOfPerson.setSurname("rodriguez");
    copyOfPerson.setYears(40);
    copy.set(0, copyOfPerson);
    System.out.println(list.get(0).getYears());
    System.out.println(copy.get(0).getYears());
    System.out.println(list.equals(copy));*/
  }

}

class Person {

  private String name;
  private String surname;
  private int years;

  public Person(String name, String surname, int years) {
    this.name = name;
    this.surname = surname;
    this.years = years;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getSurname() {
    return surname;
  }

  public void setSurname(String surname) {
    this.surname = surname;
  }

  public int getYears() {
    return years;
  }

  public void setYears(int years) {
    this.years = years;
  }

}
