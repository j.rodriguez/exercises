package exercises;

import static org.junit.jupiter.api.Assertions.assertEquals;

import lambdas.Lambda;
import lambdas.logging.DoThing;
import lambdas.logging.Logger;
import org.junit.jupiter.api.Test;


public class LambdaTest {

  @Test
  public void print_thing_method_executes_lambda_implementation() {
    //arrange
    Lambda sut = new Lambda();
    boolean expected = false;

    //act
    boolean result = sut.lambda();

    //assert
    assertEquals(expected, result);
  }

  @Test
  public void function_without_logging_doesnt_catch_exception() {
    //arrange
    DoThing sut = new DoThing(new Logger());

    //act
    sut.doThingWithoutLogging(null);

    //assert
  }

  @Test
  public void function_without_logging_wrapped_catches_exception() {
    //arrange
    DoThing sut = new DoThing(new Logger());

    //act
    sut.wrapIt(null);

    //assert
  }

}
