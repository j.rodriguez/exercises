package exercises;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import org.junit.jupiter.api.Test;

/**
 * @author jrodriguez
 */
public class BiggestPalindromeTest {
    @Test
    public void test1() {
        // arrange
        String text = "aaabbbaa";
        String expected = "aabbbaa";
        BiggestPalindrome sut = new BiggestPalindrome(text);

        // act
        String biggestPalindrome = sut.findBiggestPalindrome();

        // assert
        assertEquals(expected, biggestPalindrome);
    }

    @Test
    public void test2() {
        // arrange
        String text = "hjbbbfk";
        String expected = "bbb";
        BiggestPalindrome sut = new BiggestPalindrome(text);

        // act
        String biggestPalindrome = sut.findBiggestPalindrome();

        // assert
        assertEquals(expected, biggestPalindrome);
    }

    @Test
    public void test3() {
        // arrange
        String text = "hjkbbba";
        String expected = "bbb";
        BiggestPalindrome sut = new BiggestPalindrome(text);

        // act
        String biggestPalindrome = sut.findBiggestPalindrome();

        // assert
        assertEquals(expected, biggestPalindrome);
    }

    @Test
    public void test4() {
        // arrange
        String text = "hjkefwurfas";
        BiggestPalindrome sut = new BiggestPalindrome(text);

        // act
        String biggestPalindrome = sut.findBiggestPalindrome();

        // assert
        assertNull(biggestPalindrome);
    }
}
