package exercises;


import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

/**
 * @author jrodriguez
 */
public class PalindromeTest {

    @Test
    public void isNotPalindromeTest() {
        //setup
        String s = "Jon";

        //SUT (service under test)
        Palindrome p = new Palindrome(s);

        //assertions
        assertFalse(p.isPalindrome());
    }

    @Test
    public void isPalindromeTest() {
        //setup
        String s = "aNa";
        //SUT (service under test)

        //SUT (service under test)
        Palindrome p = new Palindrome(s);

        //assertions
        assertTrue(p.isPalindrome());
    }
}
