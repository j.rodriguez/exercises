package exercises;



import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.LinkedHashMap;
import java.util.Map;
import org.junit.jupiter.api.Test;

/**
 * @author jrodriguez
 */
public class ProcessArraysTest {

    @Test
    public void processArraysMapApproach() {
        //setup
        int[] arr = new int[]{6, 5, 2, 1, 6};
        Map<String, Integer> expectedMap = new LinkedHashMap<>();
        expectedMap.put("6", 2);
        expectedMap.put("5", 1);
        expectedMap.put("2", 1);
        expectedMap.put("1", 1);

        //SUT (Service Under Test)
        ProcessArrays processArrays = new ProcessArrays(arr);
        Map<String, Integer> serviceResponse = processArrays.processArraysMapApproach();

        //Assertions
        assertEquals(expectedMap, serviceResponse);
    }

    @Test
    public void processArraysArrayApproach() {
        //setup
        int[] arr = new int[]{6, 5, 2, 1, 6};
        int[][] expectedArray = new int[4][2];
        expectedArray[0] = new int[]{6, 2};
        expectedArray[1] = new int[]{5, 1};
        expectedArray[2] = new int[]{2, 1};
        expectedArray[3] = new int[]{1, 1};

        //SUT (Service Under Test)
        ProcessArrays processArrays = new ProcessArrays(arr);
        int[][] ints = processArrays.processArraysArrayApproach();

        //Assertions
        assertEquals(expectedArray, ints);
    }
}
