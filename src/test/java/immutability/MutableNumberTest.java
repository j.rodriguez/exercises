package immutability;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

public class MutableNumberTest {

  @Test
  void will_produce_different_results_with_multiple_calls() {
    MutableNumber sut = new MutableNumber(1);

    Assertions.assertThat(sut.sumNumber(1)).isEqualTo(2);
    Assertions.assertThat(sut.sumNumber(1)).isEqualTo(3);
  }

}
