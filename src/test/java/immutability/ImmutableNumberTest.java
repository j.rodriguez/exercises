package immutability;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

public class ImmutableNumberTest {

  @Test
  void will_produce_same_results_with_multiple_calls() {
    ImmutableNumber sut = new ImmutableNumber(1);

    Assertions.assertThat(sut.sumNumber(1)).isEqualTo(2);
    Assertions.assertThat(sut.sumNumber(1)).isEqualTo(2);
  }

}
